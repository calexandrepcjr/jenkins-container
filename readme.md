This repository stores the jenkins container + the agent bat from windows. Obviously if the container is started all over again, the bat must be changed. The idea is to show a way to easily manage the nodes and build things in jenkins.

+ Build docker image with docker compose up;
+ Generate ssh-key inside the container;
+ Add key in the Access keys inside Bitbucket;
+ Add Windows Node (or any other node) with Java Web Standard Settings (Easily sync the nodes with agent jvm files);
+ Add new project;
+ Install Unity3D + powershell plugins;
+ Config in the Manage Jenkins -> Global Tool Configuration the Unity plugin settings (generally just creating a name and leaving the rest to the specific nodes);
+ Manage Tool configuration inside the nodes to proper handle the Unity path inside the Node;
+ Build the steps inside the project;
+ That's it!
